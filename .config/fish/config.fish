set fish_greeting

export CURRENT_MANAGER="qtile"

export GOPATH=$HOME/go
export LUAROCKSPATH=$HOME/.luarocks
export CARGOPATH=$HOME/.cargo
export GEMPATH=$HOME/.gem/ruby/2.7.0
set PATH $HOME/.local/bin $GEMPATH/bin $CARGOPATH/bin $LUAROCKSPATH/bin $GOPATH/bin $PATH
export PATH

export DEVEL=$HOME/Development
export PROJECTS=$DEVEL/Projects

export XDG_DATA_HOME=$HOME/.local/share
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache

export XMONAD_DATA_DIR=$XMONAD_DATA_DIR/xmonad
export XMONAD_CONFIG_DIR=$XDG_CONFIG_HOME/xmonad
export XMONAD_CACHE_DIR=$XDG_CACHE_HOME/xmonad

export EDITOR="nvim"
export TERMINAL="alacritty"

alias exa="exa --color=always --group-directories-first"
alias ls="exa"
alias l="ls -l"
alias ll="ls -lah"

alias format-rust="find . -type d -name target -prune -o -type f -name '*.rs' -print -exec rustfmt --edition 2018 {} \;"
alias format-haskell="find . -type d -name '.stack-work' -prune -o -type f -name '*.hs' -print -exec hindent {} \; -exec stylish-haskell -i {} \; && hlint ."

alias youtube-dl-music="youtube-dl --audio-quality 0 --extract-audio --audio-format flac"
alias youtube-dl-lectures="youtube-dl -f 'bestvideo+bestaudio' --merge-output-format mp4 --audio-quality 0 -i"

alias dotfiles='git --git-dir=$PROJECTS/dotfiles/.git --work-tree=$HOME'

# Keep the ~ clean.
export LESSHISTSIZE=0

# Starship
starship init fish | source
