export CURRENT_MANAGER="qtile"

export GOPATH=$HOME/go
export LUAROCKSPATH=$HOME/.luarocks
export CARGOPATH=$HOME/.cargo
export GEMPATH=$HOME/.gem/ruby/2.7.0
export PATH=$HOME/.local/bin:$GEMPATH/bin:$CARGOPATH/bin:$LUAROCKSPATH/bin:$GOPATH/bin:/usr/local/bin:$PATH

export DEVEL=$HOME/Development
export PROJECTS=$DEVEL/Projects

export XDG_DATA_HOME=$HOME/.local/share
export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache

export XMONAD_DATA_DIR=$XMONAD_DATA_DIR/xmonad
export XMONAD_CONFIG_DIR=$XDG_CONFIG_HOME/xmonad
export XMONAD_CACHE_DIR=$XDG_CACHE_HOME/xmonad

export EDITOR=nvim

export ZSH="$HOME/.local/share/oh-my-zsh"

ZSH_THEME="gallifrey"

plugins=()

source $ZSH/oh-my-zsh.sh

alias exa="exa --color=always --group-directories-first"
alias ls="exa"

alias format-rust="find . -type d -name target -prune -o -type f -name '*.rs' -print -exec rustfmt {} \;"
alias format-haskell="find . -type d -name '.stack-work' -prune -o -type f -name '*.hs' -print -exec hindent {} \; -exec stylish-haskell -i {} \; && hlint ."

alias youtube-dl-music="youtube-dl --audio-quality 0 --extract-audio --audio-format flac"
alias youtube-dl-lectures="youtube-dl -f 'bestvideo+bestaudio' --merge-output-format mp4 --audio-quality 0 -i"

alias dotfiles='git --git-dir=$PROJECTS/dotfiles --work-tree=$HOME'

# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;      
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

source $HOME/.config/broot/launcher/bash/br
